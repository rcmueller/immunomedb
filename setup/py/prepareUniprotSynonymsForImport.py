import sys
import os

import mysql.connector as connector

from prepareUniprotFileForImport import readUniprotFile


def extractSynonyms(data):

    result = set()

    for item in data:
        if item[6] != '':
            gene = item[0]
            canonical_name = item[5]
            synonyms = item[6]
            for syn in synonyms.split(' '):
                if syn != canonical_name:
                    if syn != "" and syn != ";":
                        result.add((syn,canonical_name)) 

    # print(result)
    return set(result)

def resultAsSPimport(data):
    ''' format data for import into immunomedb using stored procedures '''

    template = 'CALL sp_insert_synonym("{}","{}","uniprot")\G' # synonym, gene_symbol

    return [template.format(*item) for item in data]


def writeResultToFile(data, filename, path="import"):
    ''' writes data as import file suitable for import into immunomedb using stored procedures '''

    PREFIX = "import_"
    filename = path + "/" + PREFIX + filename + ".sql"

    os.makedirs(os.path.dirname(filename), exist_ok=True)

    data = resultAsSPimport(data)

    with open(filename, "w+") as f:
        for item in data:
            f.write(item + "\n")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("usage: python3 prepareUniprotSynonymsForImport.py <filename>")
        sys.exit()

    print(sys.argv)

    rs = readUniprotFile(sys.argv[1])
    syn = extractSynonyms(rs)

    writeResultToFile(syn, "immunome_uniprot_synonyms", path="import")
