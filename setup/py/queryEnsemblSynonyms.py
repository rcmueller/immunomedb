import mysql.connector as mariadb


from collections import namedtuple

from EnsemblUtils import queryImmunomeDBforEnsids
from EnsemblUtils import connectionToEnsemblForSpecies
from EnsemblUtils import queryEnsemblForSpeciesInImmunomeDB

from databases import connectionToImmunome
from databases import connectionToEnsembl

SynonymData = namedtuple('SynonymData', 'ensid gene_symbol description synonym');

def queryEnsemblForSynonyms(db, ids):
    ''' queries ENSEMBL database for synonyms in table external_synonyms '''

    myquery =   "SELECT gene.stable_id, xref.display_label, xref.description, syn.synonym " \
                " FROM xref" \
                " JOIN gene ON gene.display_xref_id=xref.xref_id" \
                " JOIN external_synonym syn ON gene.display_xref_id=syn.xref_id" \
                " WHERE gene.stable_id IN ({})"

    cursor = db.cursor()
    cursor.execute(myquery.format(",".join("'{}'".format(myid) for myid in ids)))

    result  = [SynonymData(*x) for x in cursor]
    cursor.close()

    return result

def writeResultToFile(data, species, path="import"):
    ''' writes import file for import into immunome DB (using standard SQL statements) '''

    PREFIX = "import_ensembl_synonyms"
    filename = path + "/" + PREFIX + "_" + species + ".sql"

    template_sp = 'CALL sp_insert_synonym("{}","{}","ensembl - {}")\G'


    with open(filename, "w+") as f:
        for item in data:
            print(item)
            f.write(template_sp.format(item.synonym, item.gene_symbol, species) + "\n")


def queryAll():
    """ query ENSEMBL databases for synonyms (for all species in immunome DB)

        looks up spieces of immunome DB in ENSEMBL
        for all species found in ENSEMBL queries database for synonyms
        writes result to import files if synonyms found
    """

    immunomeDB = connectionToImmunome()

    species = queryEnsemblForSpeciesInImmunomeDB(immunomeDB)
    print(species)

    # species = [("gallus_gallus","99","gallus_gallus"),]
    # species = [("anser_cygnoides","99","anser_cygnoides"),]

    for (ens_species, release, imm_species) in species:

        ens_ids_with_uids = queryImmunomeDBforEnsids(immunomeDB, imm_species)
        print(ens_species + ": " + str(len(ens_ids_with_uids)))

        if len(ens_ids_with_uids)==0:
            continue;

        # ens_id_to_uid = dict([ (x[1],x[0]) for x in ens_ids_with_uids])

        ens_ids = [x[1] for x in ens_ids_with_uids]

        speciesDB = connectionToEnsemblForSpecies(ens_species, release)
        synonyms = queryEnsemblForSynonyms(speciesDB, ens_ids)
        speciesDB.close()

        print("synonyms: " + str(len(synonyms)))
        # print(synonyms)

        if len(synonyms)==0:
            continue;

        # synonym_uid = [ (x[3], ens_id_to_uid[x[0]], x[1] + ' synonym') for x in synonyms]

        # writeResultToFile(synonym_uid,imm_species)
        writeResultToFile(synonyms,imm_species)

    immunomeDB.close()


if __name__ == "__main__":
    queryAll()
