import requests
from collections import namedtuple

from databases import connectionToEnsembl

DBinfo = namedtuple('DBinfo', 'ensembl release immunome')

def queryEnsemblForSpeciesInImmunomeDB(immunomeDB):
    """ Queries ENSEMBL via REST api cor species available in immunome DB

        Returns a list of namedtuples conatining ENSEMBl species name, ENSEMBL release and immunome species name.
        The immunome species name is the ENSEMBL species name (without the infraspecific name, if present)

    """

    resp = requests.get('https://rest.ensembl.org/info/species', params={'content-type' : 'application/json'})
    if resp.status_code != 200:
        raise ApiError('GET /info/species/ {}'.format(resp.status_code))

    # Creates tupels of ENSEML DB name, ENSEMBL release and immunome species name
    result = [DBinfo(sp['name'],sp['release'],"_".join(sp['name'].rsplit("_")[0:2])) for sp in resp.json()['species']]

    species_ens = { i.ensembl : i for i in result }
    species_immu = { i.immunome : i for i in result }

    myquery = "SELECT sp_name, sp_common FROM species WHERE sp_name IN ({})"

    cursor = immunomeDB.cursor()

    cursor.execute(myquery.format(",".join("'{}'".format(r.capitalize()) for r in species_immu.keys())))

    species_found = []

    for (sp_name, sp_common) in (cursor):
        try:
            sp = species_ens[sp_name.lower()] 
        except:
            sp = species_immu[sp_name.lower()] 
        species_found.append(sp)

    cursor.close()

    return species_found


def findEnsemblDBNameforSpecies(db, species, release):
    return findEnsemblDBNameforSpecies_as_SHOW_DATABASES(db, species, release)
    # return findEnsemblDBNameforSpecies_custom(db, species, release)

def findEnsemblDBNameforSpecies_custom(db, species, release):

    number = 1

    if species == 'gallus_gallus':
        number = 6
    if species == 'aquila_chrysaetos_chrysaetos':
        number = 12
    if species == 'coturnix_japonica':
        number = 2
    if species == 'meleagris_gallopavo':
        number = 21
    if species == 'melopsittacus_undulatus':
        number = 63
    if species == 'serinus_canaria':
        number = 12
    if species == 'taeniopygia_guttata':
        number = 12
    if species == 'zonotrichia_albicollis':
        number = 101

    name = species + "_core_" + str(release) + "_" + str(number)

    return name

def findEnsemblDBNameforSpecies_as_SHOW_DATABASES(db, species, release):
    '''Looks up the database name for the given species name and release'''

    cursor = db.cursor()

    myquery =  "SHOW DATABASES LIKE '{}\_core\_{}\_%';".format(species, release)

    cursor.execute(myquery)

    result  = []

    for tmp in (cursor):
        result.append(tmp[0])

    assert cursor.rowcount == 1

    cursor.close()

    return result[0]


def connectionToEnsemblForSpecies(species, release):
    ''' connect to ENSEMBL DB for given species and release '''
    dbname = findEnsemblDBNameforSpecies(connectionToEnsembl(''), species, release)
    print("db: " + dbname)
    return connectionToEnsembl(dbname)


def queryImmunomeDBforEnsids(db, species):
    ''' queries immunomedb for tupels of uids and ensids for given species '''

    myquery = "SELECT uid, ensid FROM ensembl WHERE sp_name = '{}'";

    cursor = db.cursor()
    cursor.execute(myquery.format(species))

    result  = [x for x in cursor]
    cursor.close()

    return result




if __name__ == "__main__":
    print("EnsemblUtils.py is only a library")
