import mysql.connector as mariadb

from EnsemblUtils import queryEnsemblForSpeciesInImmunomeDB
from EnsemblUtils import queryImmunomeDBforEnsids

from EnsemblUtils import connectionToEnsemblForSpecies

from databases import connectionToImmunome
from databases import connectionToEnsembl


def queryEnsemblForTranscript(db, ids):
    ''' queries ENSEMBL database for transcript information for the given ENSEMBL ids '''

    myquery = "SELECT transcript.stable_id, gene.stable_id, seq_region.name, \
                      transcript.source, transcript.biotype, \
                      transcript.seq_region_start, transcript.seq_region_end, \
                      transcript.seq_region_strand \
               FROM transcript \
               INNER JOIN seq_region ON transcript.seq_region_id = seq_region.seq_region_id \
               INNER JOIN gene ON transcript.gene_id = gene.gene_id \
               WHERE gene.stable_id  IN ({})"

    cursor = db.cursor()
    cursor.execute(myquery.format(",".join("'{}'".format(myid) for myid in ids)))

    result  = [x for x in cursor]
    cursor.close()

    return result

def resultAsSQLimport(species, data, source="ensembl"):
    ''' formats data for import using SQL import statements '''

    entry = 'INSERT IGNORE INTO ens_transcript VALUES("{}","{}","{}","{}","{}","{}","{}","{}");'

    result = [entry.format(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7]) for d in data]

    return result

def writeResultToFile(data, species, path="import"):
    ''' writes results to import files (using SQL statements for import) '''

    PREFIX = "import_ensembl_transcript"
    filename = path + "/" + PREFIX + "_" + species + ".sql"

    data = resultAsSQLimport(species.capitalize(), data)

    with open(filename, "w+") as f:
        for item in data:
            f.write(item + "\n")


def queryEnsemblForTranscripts():
    """ extracts transcript information from ENSEMBL  for all ENSEMBL ids in immunome DB
        writes data into files suitaable for import into immunome DB
    """
    immunomeDB = connectionToImmunome()

    species = queryEnsemblForSpeciesInImmunomeDB(immunomeDB)

    for (ens_species, release, imm_species) in species:
        ids = queryImmunomeDBforEnsids(immunomeDB,imm_species)

        ids = [x[1] for x in ids]

        print(imm_species + ": " + repr(len(ids)))

        speciesDB = connectionToEnsemblForSpecies(ens_species, release)
        transcripts = queryEnsemblForTranscript(speciesDB,ids)
        writeResultToFile(transcripts,imm_species)
        speciesDB.close()

if __name__ == "__main__":
    queryEnsemblForTranscripts()
