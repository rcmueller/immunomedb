from EnsemblUtils import queryEnsemblForSpeciesInImmunomeDB
from databases import connectionToImmunome


def demo():
    immunomeDB = connectionToImmunome()
    species = queryEnsemblForSpeciesInImmunomeDB(immunomeDB)
    immunomeDB.close()

    print("species found in Ensembl:")
    for sp in species:
        print(sp)

if __name__ == "__main__":
    demo()

