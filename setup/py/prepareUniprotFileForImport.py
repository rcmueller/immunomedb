import sys

import mysql.connector as connector

from databases import connectionToImmunome


def queryImmuneDBForCanonicalGeneSymbolFromEnsemblID(ensembl_id):
    ''' returns gene symbol for given ENSEMBL id '''

    print("Retrieving gene symbol for ENSEMBL-ID {} from immunome database...".format(ensembl_id));
    db = connectionToImmunome()

    ids = ensembl_id.split(',')
    ids = ','.join(['"' + x + '"' for x in ids])

    cursor = db.cursor()
    query = " SELECT DISTINCT g.canonical_gene_symbol " \
            "FROM ensembl e " \
            "JOIN gene_symbols gs USING(uid) " \
            "JOIN gene g USING(gene_id) " \
            " WHERE  ensid IN ({}); " 
    # print(query.format(ids))
    cursor.execute(query.format(ids))

    result = cursor.fetchall();

    print("len=" + str(len(result)))
    assert len(result) == 1

    print(result[0][0] + " - " + ensembl_id)

    cursor.close()
    db.close()

    return result[0][0]


def readUniprotFile(filename):
    """ reads Uniprot file that has been downloaded from https://uniprot.org/uploadlists (column ordering is important!)

        return
    """

    print("reading file {} ...".format(filename))

    with open(filename) as f:
        header = f.readline()
        line = f.readline()

        result = []

        while line:
            # (entry, entry_name, status, protein, species, gene, synonyms, length, ensembl_id, sequence) = line.split('\t')
            (entry, entry_name, status, protein, species, gene, synonyms, length, sequence, ensembl_id) = line.strip().split('\t')
            species = "_".join(species.split(' ')[0:2])


            canonical_symbol = queryImmuneDBForCanonicalGeneSymbolFromEnsemblID(ensembl_id)
            
            if gene == "":
                gene = canonical_symbol

            if gene != canonical_symbol:
                print( "### new synonyms found ### " + gene + '<>' + canonical_symbol)

            entry = (gene, protein, species, 'uniprot', entry, canonical_symbol, synonyms)
            result.append(entry)
            line = f.readline()

        return result

def resultAsSPimport(data):
    ''' format data for import into immunomedb using stored procedures '''

    entry = 'CALL sp_insert_evidence_with_canonical("{}","{}","{}","{}","{}","{}")\G'

    #return [entry.format(*item) for item in data]
    return [entry.format(*item[0:6]) for item in data]


def writeResultToFile(data, filename, path="import"):
    ''' writes data as import file suitable for import into immunomedb using stored procedures '''

    PREFIX = "import_"
    filename = path + "/" + PREFIX + filename + ".sql"

    data = resultAsSPimport(data)

    with open(filename, "w+") as f:
        for item in data:
            f.write(item + "\n")



if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("usage: python3 prepareUniprotFileForImport.py <filename>")
        sys.exit()

    print(sys.argv)

    rs = readUniprotFile(sys.argv[1])

    writeResultToFile(rs, "immunome_ensembl2uniprot", path="import")
