import mysql.connector as mariadb

def connectionToImmunome():
    myDB = mariadb.connect(
        host="localhost",
        user="qimm",
        database="immunome"
    )
    return myDB


def connectionToEnsembl(dbname):
    db = mariadb.connect(
        host="ensembldb.ensembl.org",
        user="anonymous",
        password="",
        database=dbname
    )
    return db

