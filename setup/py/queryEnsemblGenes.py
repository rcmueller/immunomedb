import mysql.connector as mariadb
import sys
import os
import re

from databases import connectionToImmunome
from databases import connectionToEnsembl

from EnsemblUtils import queryEnsemblForSpeciesInImmunomeDB

# from EnsemblUtils import findEnsemblDBNameforSpecies
from EnsemblUtils import connectionToEnsemblForSpecies




def readGeneSymbols(filename):
    ''' reads file of gene symbols - file must contain one symbol per line '''
    symbols = []
    
    try:
        f  = open(filename, "r")
        line = f.readline()
        while line:
            symbols.append(line.strip("\n"))
            line = f.readline()
    finally:
        f.close()
    return symbols


def queryEnsemblForGenesBySymbols(db, symbols):
    """ query gene_id and name for each of the symbols given as argument

        symbols are looked up in the 'display label' and in the 'external synonym' table
    """

    myquery =   "SELECT gene.stable_id, xref.display_label, COALESCE(NULLIF(xref.description,''),gene.description,'') AS description" \
                " FROM xref" \
                " INNER JOIN gene ON xref.xref_id = gene.display_xref_id" \
                " WHERE xref.xref_id IN (" \
                "   SELECT xref_id FROM external_synonym WHERE synonym IN ({})" \
                "   UNION ALL" \
                "   SELECT xref_id FROM xref WHERE display_label IN ({})" \
                ")";

    joined_symbols = ",".join("'{}'".format(s.upper()) for s in symbols);

    cursor = db.cursor()
    cursor.execute(myquery.format(joined_symbols,joined_symbols))

    pattern=re.compile('\[.+\]')

    result  = [(*x[0:2],pattern.sub('',x[2],count=1)) for x in (cursor)]
    cursor.close()

    return result


def resultAsSPimport(species, data, source="ensembl"):
    ''' format the data dor import using immunomedb's stored procedures '''

    entry = 'CALL sp_insert_evidence("{}","{}","{}","{}","{}")\G'

    result = []

    for item in data:
        result.append(entry.format(item[1],item[2],species,source,item[0]))

    return result


def writeResultToFile(data, species, release, path="import"):
    ''' writes results to import files (using stored procedures for import) '''

    if not os.path.exists(path):
        os.makedirs(path)

    PREFIX = "import_ensembl_core"
    filename = path + "/" + PREFIX + "_" + str(release) + "_" + species + ".sql"

    data = resultAsSPimport(species.capitalize(), data)

    with open(filename, "w+") as f:
        for item in data:
            f.write(item + "\n")


def queryEnsemblSpeciesAndWriteResultsToFile(gene_symbols, species, release, sp_immunome):
    """ connects to species specific ENSEMBL DB, 
        retrieves data for gene symbols
        writes result ro file
    """

    print("connection for " + species)
    db = connectionToEnsemblForSpecies(species, release)
    result = queryEnsemblForGenesBySymbols(db, gene_symbols)
    db.close()
    writeResultToFile(result, sp_immunome, release)


def queryEnsemblForAllSpeciesinImmunome(filename):
    """ queries ENSEMBl databases for genes given as symbols in file specified by filename,
        for all species in immuonem DB that are available in ENSEMBL
    """
    genes = readGeneSymbols(filename)
    immunomeDB = connectionToImmunome()
    species = queryEnsemblForSpeciesInImmunomeDB(immunomeDB)
    immunomeDB.close()

    for sp in species:
        print("Querying {} as {} from ENSEMBL...".format(sp.immunome, sp.ensembl))
        queryEnsemblSpeciesAndWriteResultsToFile(genes, sp.ensembl, sp.release, sp.immunome)


def demo():
    GO_SEED_FILE = "data/20191216_GO_seed_gallus_unique.tsv"
    genes = readGeneSymbols(GO_SEED_FILE)
    queryEnsemblSpeciesAndWriteResultsToFile(genes, "meleagris_gallopavo", 98)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("usage: python3 queryEnsemblGenes.py <GO-file>")
        sys.exit()

    print(sys.argv)

    filename = sys.argv[1]

    queryEnsemblForAllSpeciesinImmunome(filename)
