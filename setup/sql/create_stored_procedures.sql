DELIMITER ;;
CREATE OR REPLACE PROCEDURE `sp_insert_evidence`(
  in_gene_symbol VARCHAR(20),
  in_gene_desc TEXT,
  in_sp_name VARCHAR(50),
  in_ev_name VARCHAR(50),
  in_ev_id VARCHAR(20)
)
insert_evidence:BEGIN

  DECLARE in_uid INT(11) DEFAULT 0;

  START TRANSACTION;

  IF NOT EXISTS ( SELECT sp_name FROM species WHERE sp_name = in_sp_name ) THEN
     SELECT CONCAT ( in_sp_name, ' does not exist in species; abort' ) AS 'ERROR';
     LEAVE insert_evidence;
  END IF;

  IF NOT EXISTS ( SELECT ev_name FROM source WHERE ev_name = in_ev_name ) THEN
     SELECT CONCAT ( in_ev_name, ' does not exist in evidence; abort' ) AS 'ERROR';
     LEAVE insert_evidence;
  END IF;

  IF EXISTS ( SELECT gene_symbol FROM gene_symbols WHERE gene_symbol = in_gene_symbol ) THEN
     SELECT uid INTO in_uid FROM gene_symbols WHERE gene_symbol = in_gene_symbol;
     SELECT CONCAT ( in_gene_symbol, ' relates to gene_id ', in_uid ) AS 'INFO';
  ELSEIF ( SELECT COUNT(*) FROM gene_symbols ) = 0 THEN
     SET in_uid = 1;
  ELSE
     SELECT uid + 1 INTO in_uid FROM gene_symbols AS t
      WHERE NOT EXISTS
            (
            SELECT * FROM gene_symbols
             WHERE uid = t.uid + 1
            )
   ORDER BY uid
      LIMIT 1;
     SELECT CONCAT ( in_gene_symbol, ' not present, new uid: ', in_uid ) AS 'INFO';
  END IF;

  IF NOT EXISTS ( SELECT canonical_gene_symbol FROM gene WHERE canonical_gene_symbol = in_gene_symbol ) THEN
     INSERT INTO gene(gene_id,canonical_gene_symbol,gene_desc) VALUES(in_uid,in_gene_symbol,in_gene_desc);
     SELECT 'consider updating gene.gene_desc' AS 'INFO';
  END IF;

  IF NOT EXISTS ( SELECT uid FROM gene_symbols WHERE uid = in_uid ) THEN
     INSERT INTO gene_symbols(uid, gene_symbol, gene_id) VALUES(in_uid, in_gene_symbol, in_uid);
  END IF;

  IF NOT EXISTS ( SELECT * FROM evidence WHERE uid = in_uid and ev_name = in_ev_name and sp_name = in_sp_name ) THEN
     INSERT INTO evidence VALUES(in_uid, in_ev_name, in_sp_name);
  ELSE
     SELECT CONCAT ( in_ev_id, ', ', in_gene_symbol, ': evidence "', in_uid, '-', in_ev_name, '-', in_sp_name, '" already exists; continue' ) AS 'WARNING';
  END IF;

  CASE in_ev_name
       WHEN 'ensembl' THEN
            IF NOT EXISTS ( SELECT ensid FROM ensembl WHERE ensid = in_ev_id ) THEN
               INSERT INTO ensembl VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
       WHEN 'ncbi' THEN
            IF NOT EXISTS ( SELECT accnr FROM ncbi WHERE accnr = in_ev_id ) THEN
               INSERT INTO ncbi VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
       WHEN 'b10k' THEN
            IF NOT EXISTS ( SELECT unigene FROM b10k WHERE unigene = in_ev_id ) THEN
               INSERT INTO b10k VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
       WHEN 'uniprot' THEN
            IF NOT EXISTS ( SELECT uniprotkb FROM uniprot WHERE uniprotkb = in_ev_id ) THEN
               INSERT INTO uniprot VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
  ELSE
       SELECT CONCAT ( in_ev_name, ' does not exist in evidence; abort' ) AS 'ERROR';
       ROLLBACK;
       LEAVE insert_evidence;
  END CASE;

  COMMIT;

END ;;
DELIMITER ;


DELIMITER ;;
CREATE OR REPLACE PROCEDURE `sp_insert_evidence_with_canonical`(
  in_gene_symbol VARCHAR(20),
  in_gene_desc TEXT,
  in_sp_name VARCHAR(50),
  in_ev_name VARCHAR(50),
  in_ev_id VARCHAR(20),
  in_canonical_symbol VARCHAR(20)
)
insert_evidence:BEGIN

  DECLARE in_uid INT(11) DEFAULT 0;
  DECLARE in_gene_id INT(11);

  START TRANSACTION;

  IF NOT EXISTS ( SELECT sp_name FROM species WHERE sp_name = in_sp_name ) THEN
     SELECT CONCAT ( in_sp_name, ' does not exist in species; abort' ) AS 'ERROR';
     LEAVE insert_evidence;
  END IF;

  IF NOT EXISTS ( SELECT ev_name FROM source WHERE ev_name = in_ev_name ) THEN
     SELECT CONCAT ( in_ev_name, ' does not exist in evidence; abort' ) AS 'ERROR';
     LEAVE insert_evidence;
  END IF;

  IF NOT EXISTS ( SELECT canonical_gene_symbol FROM gene WHERE canonical_gene_symbol = in_canonical_symbol ) THEN
     SELECT CONCAT ( in_canonical_symbol, ' does not exist in gene; abort' ) AS 'ERROR';
     LEAVE insert_evidence;
  ELSE
     SELECT gene_id INTO in_gene_id FROM gene WHERE canonical_gene_symbol = in_canonical_symbol;
  END IF;

  IF EXISTS ( SELECT gene_symbol FROM gene_symbols WHERE gene_symbol = in_gene_symbol ) THEN
     SELECT uid INTO in_uid FROM gene_symbols WHERE gene_symbol = in_gene_symbol;
     SELECT CONCAT ( in_gene_symbol, ' relates to gene_id ', in_uid ) AS 'INFO';
  ELSEIF ( SELECT COUNT(*) FROM gene_symbols ) = 0 THEN
     SET in_uid = 1;
  ELSE
     SELECT uid + 1 INTO in_uid FROM gene_symbols AS t
      WHERE NOT EXISTS
            (
            SELECT * FROM gene_symbols
             WHERE uid = t.uid + 1
            )
   ORDER BY uid
      LIMIT 1;
     SELECT CONCAT ( in_gene_symbol, ' not present, new uid: ', in_uid ) AS 'INFO';
  END IF;

  IF NOT EXISTS ( SELECT uid FROM gene_symbols WHERE uid = in_uid ) THEN
     INSERT INTO gene_symbols(uid, gene_symbol, gene_id) VALUES(in_uid, in_gene_symbol, in_gene_id);
  END IF;

  IF NOT EXISTS ( SELECT * FROM evidence WHERE uid = in_uid and ev_name = in_ev_name and sp_name = in_sp_name ) THEN
     INSERT INTO evidence VALUES(in_uid, in_ev_name, in_sp_name);
  ELSE
     SELECT CONCAT ( in_ev_id, ', ', in_gene_symbol, ': evidence "', in_uid, '-', in_ev_name, '-', in_sp_name, '" already exists; continue' ) AS 'WARNING';
  END IF;

  CASE in_ev_name
       WHEN 'ensembl' THEN
            IF NOT EXISTS ( SELECT ensid FROM ensembl WHERE ensid = in_ev_id ) THEN
               INSERT INTO ensembl VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
       WHEN 'ncbi' THEN
            IF NOT EXISTS ( SELECT accnr FROM ncbi WHERE accnr = in_ev_id ) THEN
               INSERT INTO ncbi VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
       WHEN 'b10k' THEN
            IF NOT EXISTS ( SELECT unigene FROM b10k WHERE unigene = in_ev_id ) THEN
               INSERT INTO b10k VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
       WHEN 'uniprot' THEN
            IF NOT EXISTS ( SELECT uniprotkb FROM uniprot WHERE uniprotkb = in_ev_id ) THEN
               INSERT INTO uniprot VALUES(in_ev_id, in_uid, in_sp_name);
            ELSE
               SELECT CONCAT ( in_ev_id, ' already present; abort' ) AS 'ERROR';
               ROLLBACK;
               LEAVE insert_evidence;
            END IF;
  ELSE
       SELECT CONCAT ( in_ev_name, ' does not exist in evidence; abort' ) AS 'ERROR';
       ROLLBACK;
       LEAVE insert_evidence;
  END CASE;

  COMMIT;

END ;;
DELIMITER ;


DELIMITER ;;
CREATE OR REPLACE PROCEDURE `sp_insert_b10k`(
  in_gene_symbol VARCHAR(10),
  in_sp_short VARCHAR(50),
  in_unigene VARCHAR(20)
)
insert_b10k:BEGIN

  DECLARE in_uid INT(11) DEFAULT 0;
  DECLARE in_sp_name VARCHAR(50);

  START TRANSACTION;

  IF NOT EXISTS ( SELECT sp_short FROM species WHERE sp_short = in_sp_short ) THEN
     SELECT CONCAT ( in_sp_short, ' absent in species, investigate' ) AS 'WARNING';
     LEAVE insert_b10k;
  ELSE
     SELECT sp_name INTO in_sp_name FROM species WHERE sp_short = in_sp_short;
  END IF;

  IF NOT EXISTS ( SELECT gene_symbol FROM gene_symbols WHERE gene_symbol = in_gene_symbol ) THEN
     SELECT CONCAT ( in_gene_symbol, ' absent in gene_symbols, investigate' ) AS 'WARNING';
     LEAVE insert_b10k;
  ELSE
     SELECT uid INTO in_uid FROM gene_symbols WHERE gene_symbol = in_gene_symbol;
     SELECT CONCAT ( in_gene_symbol, ' relates to ', in_uid ) AS 'INFO';
  END IF;

  IF NOT EXISTS ( SELECT uid FROM gene_symbols WHERE uid = in_uid ) THEN
     SELECT CONCAT ( in_uid, ' absent in gene_symbols, investigate' ) AS 'WARNING';
     LEAVE insert_b10k;
  END IF;

  IF NOT EXISTS ( SELECT * FROM evidence WHERE uid = in_uid and ev_name = 'b10k' and sp_name = in_sp_name ) THEN
     INSERT INTO evidence VALUES ( in_uid, 'b10k', in_sp_name );
  ELSE
     SELECT CONCAT ( in_uid, '-b10k-', in_sp_name, ' already present in evidence; will continue' ) AS 'INFO';
  END IF;

  IF NOT EXISTS ( SELECT unigene FROM b10k WHERE unigene = in_unigene ) THEN
     INSERT INTO b10k VALUES ( in_unigene, in_uid, in_sp_name );
  ELSE
     SELECT CONCAT ( in_unigene, ' already present in b10k; investigate' ) AS 'ERROR';
     ROLLBACK;
     LEAVE insert_b10k;
  END IF;

  COMMIT;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE OR REPLACE PROCEDURE `sp_insert_ensembl`(
  in_ev_name VARCHAR(50),
  in_species VARCHAR(50),
  in_ens_id VARCHAR(20),
  in_ens_trans_id VARCHAR(128),
  in_seq_region_start INT(10),
  in_seq_region_end INT(10),
  in_seq_region_strand TINYINT(2),
  in_biotype VARCHAR(40),
  in_gene_symbol VARCHAR(10)
)
insert_ensembl:BEGIN

  DECLARE in_uid INT(11) DEFAULT 0;

  IF NOT EXISTS ( SELECT ev_name FROM source WHERE ev_name = in_ev_name ) THEN
     SELECT 'in_ev_name does not exist in evidence, investigate' AS 'WARNING';
     LEAVE insert_ensembl;
  END IF;

  IF NOT EXISTS ( SELECT sp_name FROM species WHERE sp_name = in_species ) THEN
     SELECT 'in_species does not exist in species, investigate' AS 'WARNING';
     LEAVE insert_ensembl;
  END IF;

  IF EXISTS ( SELECT ensid FROM ensembl WHERE ensid = in_ens_id ) THEN
     SELECT * FROM ensembl WHERE ensid = in_ens_id;
     SELECT 'in_ens_id already exists in ensembl, investigate' AS 'WARNING';
     LEAVE insert_ensembl;
  END IF;

  IF EXISTS ( SELECT ens_trans_id FROM ens_transcript WHERE ens_trans_id = in_ens_trans_id ) THEN
     SELECT * FROM ens_transcript WHERE ens_trans_id = in_ens_trans_id;
     SELECT 'in_ens_trans_id already exists in ens_transcript, investigate' AS 'WARNING';
     LEAVE insert_ensembl;
  END IF;

  IF EXISTS ( SELECT gene_symbol FROM gene_symbols WHERE gene_symbol = in_gene_symbol ) THEN
     SELECT uid INTO in_uid FROM gene_symbols WHERE gene_symbol = in_gene_symbol;
     SELECT CONCAT ( 'in_gene_symbol already exists in gene_symbols, retrieving corresponding uid: ', in_uid ) AS 'INFO';
  ELSE
     SELECT MAX(uid) + 1 INTO in_uid FROM gene;
     SELECT CONCAT ( 'in_gene_symbol does not exist in gene_symbols, incrementing max(uid): ', in_uid ) AS 'INFO';
  END IF;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE OR REPLACE PROCEDURE `sp_insert_mapping`(
  IN `unique_id` INT,
  IN `evidence_name` VARCHAR(50),
  IN `species_name` VARCHAR(50)
)
BEGIN

  IF NOT EXISTS(SELECT uid FROM gene_symbols where uid = unique_id) THEN
     INSERT INTO gene_symbols (uid) VALUES(unique_id);
     SELECT unique_id = LAST_INSERT_ID();
  ELSE
     SELECT 'uid exists in gene_symbols' AS 'INFO';
  END IF;

  IF NOT EXISTS(SELECT ev_name FROM source WHERE ev_name = evidence_name) THEN
     INSERT INTO source (ev_name) VALUES(evidence_name);
     SELECT evidence_name = LAST_INSERT_ID();
  ELSE
     SELECT 'ev_name exists in source' AS 'INFO';
  END IF;

  IF NOT EXISTS(SELECT sp_name FROM species WHERE sp_name = species_name) THEN
     INSERT INTO species (sp_name) VALUES(species_name);
     SELECT species_name = LAST_INSERT_ID();
  ELSE
     SELECT 'sp_name exists in species' AS 'INFO';
  END IF;

  INSERT INTO evidence (uid, sp_name, ev_name)
     VALUES(unique_id, species_name, evidence_name);

END ;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE PROCEDURE `sp_insert_synonym`(
  IN `in_synonym` VARCHAR(20),
  IN `in_gene_symbol` VARCHAR(20),
  IN `in_source` VARCHAR(50)
)
BEGIN

  SELECT CONCAT('inserting synonym ', in_synonym, ' for gene_symbol ', in_gene_symbol) AS 'INFO';

  IF NOT EXISTS (SELECT gene_symbol FROM gene_symbols WHERE gene_symbol = in_synonym) THEN
     SELECT CONCAT('synonym ', in_synonym, ' for ', in_gene_symbol, ' not found in gene_symbols - inserting') AS 'INFO';
     INSERT INTO gene_symbols(uid, gene_symbol, gene_id, src_synonym)
         SELECT (SELECT 1+MAX(uid) from gene_symbols), in_synonym, gene_id, 
	           CONCAT(in_source, '(', in_synonym, '->', in_gene_symbol, ')' )
		FROM gene WHERE canonical_gene_symbol=in_gene_symbol;
  ELSEIF EXISTS (SELECT gene_symbol FROM gene_symbols JOIN gene USING(gene_id) WHERE gene_symbol = in_synonym AND canonical_gene_symbol=in_gene_symbol) THEN
     SELECT CONCAT('synonym ', in_synonym, ' for ', in_gene_symbol, ' already found in gene_symbols - nothing to do') AS 'INFO';
  ELSE
     SELECT CONCAT('synonym ', in_synonym, ' for ', in_gene_symbol, ' found in gene_symbols - updating gene_id') AS 'INFO';
     UPDATE gene_symbols orig
        SET gene_id = (SELECT gene_id
                       FROM gene
                      WHERE canonical_gene_symbol = in_gene_symbol),
	    src_synonym = (SELECT CONCAT(in_source, '(', in_synonym, '->', in_gene_symbol, ')', 
			IFNULL(CONCAT(',', gs.src_synonym),'' ) )
                       FROM gene_symbols gs
                      WHERE gs.uid=orig.uid
		)
      WHERE gene_id IN (SELECT gs.gene_id
                          FROM (SELECT * FROM gene_symbols) AS gs
                         WHERE gs.gene_symbol = in_synonym);
  END IF;

	

END ;;
DELIMITER ;

SHOW PROCEDURE STATUS;
