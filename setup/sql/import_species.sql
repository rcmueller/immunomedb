
LOAD DATA LOCAL INFILE  'data/Sample_sheet_363genomes_short_name.txt'
INTO TABLE species
FIELDS TERMINATED BY '\t'
IGNORE 1 LINES
(sp_short, @dummy, sp_order, sp_family, sp_name, sp_common)
SET sp_class="Aves", sp_genus=SUBSTRING_INDEX(sp_name, " ", 1), sp_name=REPLACE(sp_name, " ", "_"), sp_family=TRIM(sp_family)
;


UPDATE species SET sp_common = 'Chestnut-crowned Babbler'
               WHERE sp_name = 'Pomatostomus_ruficeps' AND sp_common='';

UPDATE species SET sp_common = 'Siberian stonechat'
               WHERE sp_name = 'Saxicola_maurus' AND sp_common='';

UPDATE species SET sp_common = 'Tawny-bellied Seedeater'
               WHERE sp_name = 'Sporophila_hypoxantha' AND sp_common='';

UPDATE species SET sp_common = 'Oilbird'
               WHERE sp_name = 'Steatornis_caripensis' AND sp_common='';

UPDATE species SET sp_name = 'Anser_cygnoides'
               WHERE sp_name = 'Anser_cygnoid'
