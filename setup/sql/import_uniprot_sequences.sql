
LOAD DATA LOCAL INFILE  'data/20200311_ENSEMBL_IDs_to_UniProtKB_mapping.tsv'
INTO TABLE uni_seq
FIELDS TERMINATED BY '\t'
IGNORE 1 LINES
(uniprotkb, entry_name, status, @dummy, @dummy, @dummy, @dummy, `length`, sequence, @dummy)
;
