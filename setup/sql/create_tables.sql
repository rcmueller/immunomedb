CREATE TABLE `source` (
  `ev_name` VARCHAR(50) NOT NULL,
  `version` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`ev_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `gene` (
  `gene_id` int(11) NOT NULL,
  `canonical_gene_symbol` VARCHAR(20) NOT NULL,
  `gene_desc` TEXT,
  PRIMARY KEY (`canonical_gene_symbol`),
  UNIQUE `gene_id` (`gene_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `gene_symbols` (
  `uid` INT(11) NOT NULL,
  `gene_symbol` VARCHAR(20) NOT NULL,
  `gene_id` INT(11) NOT NULL,
  `src_synonym` VARCHAR(250) DEFAULT NULL,
  PRIMARY KEY (`gene_symbol`),
  UNIQUE (`uid`),
  CONSTRAINT `fk_gene_symbols_gene` FOREIGN KEY (`gene_id`) REFERENCES `gene` (`gene_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `species` (
  `sp_name` VARCHAR(50) NOT NULL,
  `sp_genus` VARCHAR(50) DEFAULT NULL,
  `sp_family` VARCHAR(100) DEFAULT NULL,
  `sp_order` VARCHAR(100) DEFAULT NULL,
  `sp_class` VARCHAR(100) DEFAULT NULL,
  `sp_short` VARCHAR(6) DEFAULT NULL,
  `sp_common` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`sp_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `evidence` (
  `uid` int(11) NOT NULL,
  `ev_name` VARCHAR(50) NOT NULL DEFAULT '',
  `sp_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`uid`,`sp_name`,`ev_name`),
  INDEX `sp_name` (`sp_name`),
  INDEX `src_name` (`ev_name`),
  CONSTRAINT `fk_evidence_gene` FOREIGN KEY (`uid`) REFERENCES `gene_symbols` (`uid`),
  CONSTRAINT `fk_evidence_species` FOREIGN KEY (`sp_name`) REFERENCES `species` (`sp_name`),
  CONSTRAINT `fk_evidence_source` FOREIGN KEY (`ev_name`) REFERENCES `source` (`ev_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `ensembl` (
  `ensid` VARCHAR(20) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `sp_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`ensid`),
  KEY `ensembl_uid` (`uid`),
  CONSTRAINT `fk_ensembl_gene` FOREIGN KEY (`uid`) REFERENCES `gene_symbols` (`uid`),
  CONSTRAINT `fk_ensembl_species` FOREIGN KEY (`sp_name`) REFERENCES `species` (`sp_name`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `ens_transcript` (
  `ens_trans_id` VARCHAR(20) NOT NULL DEFAULT '',
  `ensid` VARCHAR(20) DEFAULT NULL,
  `seqname` VARCHAR(255) DEFAULT NULL,
  `source` VARCHAR(40) DEFAULT NULL,
  `biotype` VARCHAR(40) DEFAULT NULL,
  `seq_region_start` int(10) DEFAULT NULL,
  `seq_region_end` int(10) DEFAULT NULL,
  `seq_region_strand` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`ens_trans_id`),
  KEY `ensid` (`ensid`),
  CONSTRAINT `fk_ens_transcript_ensembl` FOREIGN KEY (`ensid`) REFERENCES `ensembl` (`ensid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `b10k` (
  `unigene` VARCHAR(20) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `sp_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`unigene`),
  KEY `b10k_uid` (`uid`),
  CONSTRAINT `fk_b10k_gene` FOREIGN KEY (`uid`) REFERENCES `gene_symbols` (`uid`),
  CONSTRAINT `fk_b10k_species` FOREIGN KEY (`sp_name`) REFERENCES `species` (`sp_name`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `b10k_gff` (
  `unigene` VARCHAR(20) NOT NULL,
  `seqid` VARCHAR(50) NOT NULL,
  `source` VARCHAR(20) DEFAULT NULL,
  `type` VARCHAR(20) DEFAULT NULL,
  `start` int(10) NOT NULL,
  `end` int(10) NOT NULL,
  `score` VARCHAR(5) DEFAULT NULL,
  `strand` VARCHAR(1) DEFAULT NULL,
  `phase` VARCHAR(1) DEFAULT NULL,
  `attribute` TINYTEXT,
  `nt_seq` MEDIUMTEXT,
  INDEX `unigene` (`unigene`),
  PRIMARY KEY (`unigene`,`seqid`,`start`,`end`),
  CONSTRAINT `fk_b10k_gff_b10k` FOREIGN KEY (`unigene`) REFERENCES `b10k` (`unigene`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE INDEX b10k_gff_seq_idx ON b10k_gff(seqid,start,end,unigene);



CREATE TABLE `uniprot` (
  `uniprotkb` VARCHAR(20) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `sp_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`uniprotkb`),
  KEY `uniprot_uid` (`uid`),
  CONSTRAINT `fk_uniprot_gene` FOREIGN KEY (`uid`) REFERENCES `gene_symbols` (`uid`),
  CONSTRAINT `fk_uniprot_species` FOREIGN KEY (`sp_name`) REFERENCES `species` (`sp_name`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `uni_seq` (
  `entry_name` VARCHAR(20) NOT NULL,
  `uniprotkb` VARCHAR(20) DEFAULT NULL,
  `status` VARCHAR(20) DEFAULT NULL,
  `length` smallint(5) unsigned DEFAULT NULL,
  `sequence` TEXT,
  PRIMARY KEY (`entry_name`),
  KEY `uniprotkb` (`uniprotkb`),
  CONSTRAINT `fk_uni_seq_uniprot` FOREIGN KEY (`uniprotkb`) REFERENCES `uniprot` (`uniprotkb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SHOW TABLES;
