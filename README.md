## Query IMMunome db script

Query records from the Avian Immunome DB.
Needs mysql.connector installed:
```
sudo apt install python3-mysql.connector
```

### General usage and help

The script qimm.py provides six options which reflect queries against the
specified tables of the Avian Immunome DB. Just calling the script or the script
with an option shows a short usage message. With the -h argument, a detailed
help message is displayed.

- General usage message of the script:
  ```shell
  ./qimm.py
  ```
  ```
  usage: qimm.py [-h] [--version]
                 {b10k,uniprot,ensembl,evidence,species,gene} ...
  ```

- Detailed help message of the script:
  ```shell
  ./qimm.py -h
  ```
  ```
  usage: qimm.py [-h] [--version]
                 {b10k,uniprot,ensembl,evidence,species,gene} ...
  
  Query Avian Immunome DB
  
  optional arguments:
    -h, --help            show this help message and exit
    --version             show program's version number and exit
  
  subcommands:
    pick one:
  
    {b10k,uniprot,ensembl,evidence,species,gene}
      b10k                help: ./qimm.py b10k -h
      uniprot             help: ./qimm.py uniprot -h
      ensembl             help: ./qimm.py ensembl -h
      evidence            help: ./qimm.py evidence -h
      species             help: ./qimm.py species -h
      gene                help: ./qimm.py gene -h
  
  Sub-commands are mutually exclusive and accept one out of species (-s), family
  (-f) or order (-o). SQL wildcard (%) is accepted.
  ```

- General usage message for, e.g., option b10k:
  ```shell
  ./qimm.py b10k
  ```
  ```
  usage: qimm.py b10k [-h]
                      (-s SPECIES [SPECIES ...] | -f FAMILY [FAMILY ...] | -o ORDER [ORDER ...])
                      [-v] [-g GENE] [-t {CDS,mRNA}] [-w]
  qimm.py b10k: error: one of the arguments -s/--species -f/--family -o/--order is required
  ```

- Detailed help message for, e.g., option b10k:
  ```shell
  ./qimm.py b10k -h
  ```
  ```
  usage: qimm.py b10k [-h]
                      (-s SPECIES [SPECIES ...] | -f FAMILY [FAMILY ...] | -o ORDER [ORDER ...])
                      [-v] [-g GENE] [-t {CDS,mRNA}] [-w]
  
  optional arguments:
    -h, --help            show this help message and exit
    -s SPECIES [SPECIES ...], --species SPECIES [SPECIES ...]
    -f FAMILY [FAMILY ...], --family FAMILY [FAMILY ...]
    -o ORDER [ORDER ...], --order ORDER [ORDER ...]
    -v, --verbose         increase output verbosity
    -g GENE, --gene GENE  gene symbol, default: TLR3
    -t {CDS,mRNA}, --type {CDS,mRNA}
                          default: mRNA
    -w, --write           generate output files
  ```

The options are mutually exclusive and provide queries against core (evidence,
species, gene) or feature tables (b10k, uniprot, ensembl). If the database
contains alternative gene symbols (gene names), they are printed at the end.
The SQL wildcard (%) is allowed for species, families and orders. If the write
argument (-w) is provided with the option b10k or uniprot, output files will be
generated in fasta format.

### Examples

- Query evidence table to get an overview (all orders using wildcard). For gene
  CCL26, which species and what kind of evidence are there in the Avian Immunome DB:
  ```shell
  ./qimm.py evidence -o % -g CCL26
  ```
  ```
  gene	description	evidence	order	family	species	common	short
  CCL26	Chemokine	b10k	Accipitriformes	Accipitridae	Haliaeetus_albicilla	White-tailed Sea Eagle	HALALB
  CCL26	Chemokine	b10k	Accipitriformes	Accipitridae	Haliaeetus_leucocephalus	Bald Eagle	HALLEU
  CCL26	Chemokine	b10k	Charadriiformes	Chionidae	Chionis_minor	Black-faced Sheathbill	CHIMIN
  CCL26	Chemokine	uniprot	Galliformes	Phasianidae	Gallus_gallus	Chicken (Red Junglefowl)	GALGAL
  CCL26	Chemokine	b10k	Passeriformes	Callaeidae	Callaeas_wilsoni	North Island Kokako	CALWIL
  CCL26	Chemokine	b10k	Passeriformes	Malaconotidae	Dryoscopus_gambensis	Northern Puffback	DRYGAM
  CCL26	Chemokine	b10k	Passeriformes	Maluridae	Malurus_elegans	Red-winged Fairywren	MALELE
  CCL26	Chemokine	b10k	Passeriformes	Monarchidae	Myiagra_hebetior	Velvet Myiagra	MYIHEB
  CCL26	Chemokine	b10k	Passeriformes	Oriolidae	Oriolus_oriolus	Eurasian Golden Oriole	ORIORI
  CCL26	Chemokine	b10k	Passeriformes	Pomatostomidae	Pomatostomus_ruficeps	Chestnut-crowned Babbler	POSRUF
  CCL26	Chemokine	b10k	Passeriformes	Rhipiduridae	Chaetorhynchus_papuensis	Pygmy Drongo	CHAPAP
  CCL26	Chemokine	b10k	Pelecaniformes	Balaenicipitidae	Balaeniceps_rex	Shoebill	BALREX
  CCL26	Chemokine	b10k	Pelecaniformes	Phalacrocoracidae	Phalacrocorax_auritus	Double-crested Cormorant	NANAUR
  CCL26	Chemokine	b10k	Pelecaniformes	Phalacrocoracidae	Phalacrocorax_brasilianus	Neotropic Cormorant	NANBRA
  CCL26	Chemokine	b10k	Pelecaniformes	Phalacrocoracidae	Phalacrocorax_carbo	Great Cormorant	PHACAR
  CCL26	Chemokine	b10k	Pelecaniformes	Phalacrocoracidae	Phalacrocorax_harrisi	Flightless Cormorant	NANHAR
  CCL26	Chemokine	b10k	Pelecaniformes	Phalacrocoracidae	Phalacrocorax_pelagicus	Pelagic Cormorant	URIPEL
  CCL26	Chemokine	b10k	Piciformes	Galbulidae	Galbula_dea	Paradise Jacamar	GALDEA
  
  Alternative gene symbols (names) found for CCL26:
  
  gene symbol	gene description:
  CCLi10		CCL26 synonym (uid 948)
  
  
  ```

- Query b10k, order Pelecaniformes, gene CCL26
  ```shell
  ./qimm.py b10k -o Pele% -g CCL26
  ```
  ```
  >Balaeniceps_rex CCL26_BALREX_R09441_scaffold11393_mRNA
  ATGAAGGTCTTCTCCTTGGCCCTGCTCACCCTGCTGCTGGTGGCTCTCTGGACTG [... output truncated]
  >Phalacrocorax_auritus CCL26_NANAUR_R02015_NFMC01000853.1_mRNA
  CACGGAGGTGGTGGttgatttcttctgctgcaggtACTCCTGGAACCAGCCCTCC [... output truncated]
  >Phalacrocorax_brasilianus CCL26_NANBRA_R13269_NFME01057717.1_mRNA
  ATGAAGGTCTTCCCCTTGGCTCTGctcaccctgctgctggtggctctCTGGACTG [... output truncated]
  >Phalacrocorax_harrisi CCL26_NANHAR_R09653_NEVG01000017.1_mRNA
  ATGAAGGTCTTCCCCTTGGCTCTGctcaccctgctgctggtggctctCTGGACTG [... output truncated]
  >Phalacrocorax_carbo CCL26_PHACAR_R10303_scaffold32698_mRNA
  CATGGAGGTGGTGGTTGATTTCTTCTGCTGCAGGTACTCCTGGAACCAGCCCTCC [... output truncated]
  >Phalacrocorax_pelagicus CCL26_URIPEL_R05416_NFMD01000639.1_mRNA
  ATGAAGGTCTTCCCCTTGGCTCTGctcaccctgctgctggtggctctCTGGACTG [... output truncated]
  
  Alternative gene symbols (names) found for CCL26:
  
  gene symbol	gene description:
  CCLi10		CCL26 synonym (uid 948)
  
  
  ```

- Query uniprot, species duck and chicken, gene NFKB2, unreviewed (TrEMBL) entries only 
  ```shell
  ./qimm.py uniprot -s Anas% Gallus% -g NFKB2 -c u
  ```
  ```
  >Peking_Duck NFKB2_A0A493SV05_A0A493SV05_ANAPP_unreviewed_845
  MMEIMKEKLKQQKIRNKNSLLTEAELREIELEAKELKKVMDLSIVRLRFTAYLRDSS [... output truncated]
  >Peking_Duck NFKB2_A0A493TGR7_A0A493TGR7_ANAPP_unreviewed_770
  MMEIMKEKLKQQKIRNKNSLLTEAELREIELEAKELKKVMDLSIVRLRFTAYLRDSS [... output truncated]
  >Peking_Duck NFKB2_U3J1A4_U3J1A4_ANAPP_unreviewed_743
  MMEIMKEKLKQQKIRNKNSLLTEAELREIELEAKELKKVMDLSIVRLRFTAYLRDSS [... output truncated]
  >Chicken_(Red_Junglefowl) NFKB2_A0A1D5P2B7_A0A1D5P2B7_CHICK_unreviewed_905
  MDEHFQPCLDGIDYDDFSFGSHMVEQKEPLMETAVGPYLVIIEQPKQRGFRFRYGCE [... output truncated]
  >Chicken_(Red_Junglefowl) NFKB2_R4GIS9_R4GIS9_CHICK_unreviewed_913
  MLGLDGLLRPAASGTCLDGIDYDDFSFGSHMVEQKEPLMETAVGPYLVIIEQPKQRG [... output truncated]
  ```

## Citations

Currently, most of the data come from [The Bird 10,000 Genomes (B10K) Project](https://b10k.genomics.cn/index.html).
Any publications utilising data from the Avian Immunome DB project must cite
[Mueller et al., 2020. Avian Immunome DB: an example of a user-friendly interface for extracting genetic information. BMC Bioinformatics 21.](https://doi.org/10.1186/s12859-020-03764-3)
and
[Feng et al.,2020. Dense sampling of bird diversity increases power of comparative genomics. Nature.](https://doi.org/10.1038/s41586-020-2873-9)
