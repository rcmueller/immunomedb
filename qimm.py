#!/usr/bin/python3

###############################################################################
# DESCRIPTION:
###############################################################################
# b10k:     Query nucleotide sequences of one/many species, families, orders.
#           For type CDS, bin multiple CDS per species in corresponding bin.
#           ! note: relies on CDS being in the same order for all species
#           ! and header for each CDS per species must be identical
#           (call aligner) (tbd?)
#           (show alignment) (tbd?)
# uniprot:  Query amino acid sequences, either reviewed (SwissProt) or
#           unreviewed (TrEMBL).
# ensembl:  Query transcript information for alternative splice variants.
# evidence: Query where evidence has been seen for a given gene.
# species:  Query details about species names etc.
# gene:     Look up alternative gene names. Also default in all sub-commands
#           except for species.
###############################################################################
# STRUCTURE:
###############################################################################
# Import modules
# Parser (top-level, parent, childs)
# Global variables
# DB connection
# Function: create target (species, families, orders)
# Function: execute query, collect results
# Main
###############################################################################


###############################################################################
# Import db connector library ... and others
###############################################################################
import argparse
import mysql.connector as mariadb
import os
import sys
import time


###############################################################################
# Top-level parser
###############################################################################
def parseArguments():
    parser = argparse.ArgumentParser(description='Query Avian Immunome DB',
                                     epilog='Sub-commands are mutually exclusive \
                                             and accept one out of species (-s), \
                                             species common name (-c), \
                                             genus (-n), \
                                             family (-f), or order (-o). \
                                             SQL wildcard (%) is accepted.')
    parser.add_argument('--version', action='version', version='%(prog)s v0.2')
    # subparsers = parser.add_subparsers(dest='sub_command', metavar='pick one:')
    subparsers = parser.add_subparsers(dest='sub_command', description='pick one:')
    
    ###############################################################################
    # Parent parser (common elements, inherited by child parsers)
    ###############################################################################
    parent_parser = argparse.ArgumentParser(add_help=False)
    group = parent_parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-s', '--species', nargs='+', action='store')
    group.add_argument('-m', '--spcommon', nargs='+', action='store')
    group.add_argument('-n', '--genus', nargs='+', action='store')
    group.add_argument('-f', '--family', nargs='+', action='store')
    group.add_argument('-o', '--order', nargs='+', action='store')
    group.add_argument('-C', '--spclass', nargs='+', action='store')
    parent_parser.add_argument('-v', '--verbose', action='store_true',
                               help='increase output verbosity')
    
    ###############################################################################
    # Subparser for ...
    ###############################################################################
    # B10k
    b10k_parser = subparsers.add_parser('b10k', parents=[parent_parser],
                                        help='help: ./%(prog)s b10k -h')
    b10k_parser.add_argument('-g', '--gene', default='IFNL', action='store',
                             help='gene symbol, default: TLR3')
    b10k_parser.add_argument('-t', '--type', choices=['CDS', 'mRNA'], default='mRNA',
                             action='store', help='default: mRNA')
    b10k_parser.add_argument('-w', '--write', action='store_true',
                             help='generate output files')
    # UniProt
    uniprot_parser = subparsers.add_parser('uniprot', parents=[parent_parser],
                                           help='help: ./%(prog)s uniprot -h')
    uniprot_parser.add_argument('-g', '--gene', default='IFNL', action='store',
                                help='gene symbol, default: TLR3')
    uniprot_parser.add_argument('-c', '--credibility', choices=['a', 'r', 'u'],
                                default='a', action='store',
                                help='credibility (all/reviewed/unreviewed), default: all')
    uniprot_parser.add_argument('-w', '--write', action='store_true',
                                help='generate output files')
    # ENSEMBL
    ensembl_parser = subparsers.add_parser('ensembl', parents=[parent_parser],
                                           help='help: ./%(prog)s ensembl -h')
    ensembl_parser.add_argument('-g', '--gene', default='IFNL', action='store',
                                help='gene symbol, default: TLR3')
    # Evidence
    evidence_parser = subparsers.add_parser('evidence', parents=[parent_parser],
                                            help='help: ./%(prog)s evidence -h')
    evidence_parser.add_argument('-g', '--gene', default='IFNL', action='store',
                                 help='gene symbol, default: TLR3')
    # Species
    species_parser = subparsers.add_parser('species', parents=[parent_parser],
                                           help='help: ./%(prog)s species -h')
    # Gene
    gene_parser = subparsers.add_parser('gene', help='help: ./%(prog)s gene -h')
    gene_parser.add_argument('-g', '--gene', default='IFNL', action='store',
                             help='gene symbol, default: IFNL')
    gene_parser.add_argument('-v', '--verbose', action='store_true',
                             help='increase output verbosity')
    
    if len(sys.argv) == 1:
        # parser.print_help(sys.stderr)
        parser.print_usage(sys.stderr)
        sys.exit(1)
    
    args = parser.parse_args()
    return args

###############################################################################
# Function: create target (species, families, orders)
###############################################################################
def createTarget(args):
    myTarget = []    # query part depending on argument species, genus, family or order
    myFilePart = ""
    # create SQL part for target (one or many species/families/orders)
    if args.species:
        for mySpecies in args.species:
            myTarget.append("(sp_name LIKE '" + mySpecies + "') ")
            myFilePart = "{0}{1}_".format(myFilePart, mySpecies)

    if args.spcommon:
        for mySpecies in args.spcommon:
            myTarget.append("(sp_common LIKE '" + mySpecies + "') ")
            myFilePart = "{0}{1}_".format(myFilePart, mySpecies)

    if args.genus:
        for myGenus in args.genus:
            myTarget.append("sp_genus LIKE '" + myGenus + "'")
            myFilePart = "{0}{1}_".format(myFilePart, myGenus)

    if args.family:
        for myFamily in args.family:
            myTarget.append("sp_family LIKE '" + myFamily + "'")
            myFilePart = "{0}{1}_".format(myFilePart, myFamily)

    if args.order:
        for myOrder in args.order:
            myTarget.append("sp_order LIKE '" + myOrder + "'")
            myFilePart = "{0}{1}_".format(myFilePart, myOrder)

    if args.spclass:
        for myClass in args.spclass:
            myTarget.append("sp_class LIKE '" + myClass + "'")
            myFilePart = "{0}{1}_".format(myFilePart, myClass)

    if args.verbose:
        print("\ntarget string:", myTarget)

    return (myTarget, myFilePart)


###############################################################################
# Function: execute query, collect results
###############################################################################

def openDbConnection():
    myDB = mariadb.connect(
        host="avimm.ab.mpg.de",
        # host="localhost",
        user="qimm",
        database="immunome"
    )
    return myDB

def queryAndResults(args, myQuery):
    myQueryResultsPart = []    # collect DB records for each query

    if args.verbose:
        print("\nquery:", " ".join(myQuery.split()))


    myDB = openDbConnection()
    myCursor = myDB.cursor()
    # execute myQuery, capture potential db errors
    try:
        myCursor.execute(myQuery)
    except mariadb.Error as error:
        print("Error: {}".format(error))

    # collect records for this query
    for myLine in (myCursor):
        myQueryResultsPart.append(myLine)

    myCursor.close()
    myDB.close()

    # convert list of tuples to list of lists
    # (because tuples are immutable, items cannot be altered nor appended)
    myQueryResultsPart = [list(row) for row in myQueryResultsPart]

    return (myQueryResultsPart)


###############################################################################
# B10k
###############################################################################

def queryB10k(args):
    myGene = args.gene
    myType = args.type
    myTarget, myFilePart = createTarget(args)
    myQueryResults = []    # collect DB records of each query

    # prepare query for each element in myTarget
    for myTaxa in myTarget:
        myQuery = "SELECT CONCAT( \
                                 '>', \
                                 REPLACE(sp_name, ' ', '_'), \
                                 ' ', canonical_gene_symbol, \
                                 '_', unigene, \
                                 '_', seqid, \
                                 '_', type) \
                              AS fasta_header, nt_seq \
                              , '' AS idx \
                              , canonical_gene_symbol, sp_name, unigene, seqid, source, type, start, end, score, strand, phase, attribute \
                     FROM b10k_gff \
                     JOIN b10k USING (unigene) \
                     JOIN gene_symbols USING (uid) \
                     JOIN gene USING (gene_id) \
                     JOIN gene_symbols gs USING (gene_id) \
                     JOIN species USING (sp_name) \
                    WHERE gs.gene_symbol LIKE '{0}' \
                      AND type = '{1}' \
                      AND {2} \
                    ORDER BY sp_name, canonical_gene_symbol, unigene, seqid;".format(myGene, myType, myTaxa)

        # execute query and collect results
        myQueryResults.extend(queryAndResults(args, myQuery))

    if not myQueryResults:
        return myQueryResults

    # print (write) output
    # post processinging: if -t is CDS, add suffix (increment CDS numbers)
    # for each species alternatively: append index to list
    myCDS = 0    # if aurgument -t CDS, put CDS in different bins
    myIndex = 0
    if args.type == "CDS":
        # first fasta header in records is stored in list[0][0]
        myLine = myQueryResults[myCDS][0]
        for myCDS in range(0,len(myQueryResults)):
            if myQueryResults[myCDS][0] == myLine:
                myIndex += 1
                myQueryResults[myCDS][2]=str(myIndex)
            else:
                myIndex = 1
                myLine = myQueryResults[myCDS][0]
                myQueryResults[myCDS][2]=str(myIndex)
            myQueryResults[myCDS][0] += '_' + str(myQueryResults[myCDS][9]) + ':' + str(myQueryResults[myCDS][10])

    # print output depending on mRNA/CDS
    if args.type == "mRNA":
            # , gene, sp_name, unigene, seqid, source, type, start, end, score, strand, phase, attribute \
        for fasta_header, nt_seq, _,  *_unused in myQueryResults:
            print("{0}\n{1}".format(fasta_header, nt_seq))
    else:
        # sort results based on fasta header and index (key is a function; lambda defines one-line function)
        # myQueryResults = sorted(myQueryResults, key=lambda x: (x[0],int(x[2])))
        for fasta_header, nt_seq, index, *_unused in myQueryResults:
            print("{0}\n{1}".format(fasta_header, nt_seq))

    if args.write:
        writeResultAsFasta(args, myQueryResults, myDir, myFilePart)

    return myQueryResults

def writeResultAsFasta(args, myQueryResults, myDir, myFilePart):

    if args.type == "mRNA":
        for fasta_header, nt_seq, _ , *_unused in myQueryResults:
            myFile = "{0}/{1}mRNA".format(myDir, myFilePart)
            file = open(myFile, "a")
            file.write("{0}\n{1}\n".format(fasta_header, nt_seq))
            file.close()

    if args.type == "CDS":
        for fasta_header, nt_seq, myIndex , *_unused in myQueryResults:
            myFile = "{0}/{1}CDS_{2}".format(myDir, myFilePart, myIndex)
            file = open(myFile, "a")
            file.write("{0}\n{1}\n".format(fasta_header, nt_seq))
            file.close()


###############################################################################
# UniProt
###############################################################################

def queryUniProt(args):
    myGene = args.gene
    myCred = args.credibility
    myTarget, myFilePart = createTarget(args)
    myQueryResults = []    # collect DB records of each query

    # convert myCred to SQL
    if myCred:
        if myCred == "r":
            myCred = "reviewed"
        elif myCred == "u":
            myCred = "unreviewed"
        else:
            myCred = "%"

    # prepare query for each element in myTarget
    # beware: allowing "gene wildcards" (LIKE instead of =) leads to inflated
    # results set due to alternative gene symbols and the JOIN with gene_name;
    # alternative JOIN:
    # JOIN (select * from gene_name group by uid) as b on uniprot.uid = b.uid
    for myTaxa in myTarget:
        myQuery = "SELECT CONCAT( \
                                 '>',\
                                 REPLACE(sp_name, ' ', '_'), \
                                 ' ', canonical_gene_symbol, \
                                 '_', uniprotkb, \
                                 '_', entry_name, \
                                 '_', status, \
                                 '_', length) \
                              AS fasta_header, sequence \
                              , canonical_gene_symbol AS gene, sp_name, uniprotkb, entry_name, status, length \
                     FROM uni_seq \
                     JOIN uniprot USING (uniprotkb) \
                     JOIN gene_symbols USING (uid) \
                     JOIN gene USING (gene_id) \
                     JOIN gene_symbols gs USING (gene_id) \
                     JOIN species USING (sp_name) \
                    WHERE gs.gene_symbol LIKE '{0}' \
                      AND {1} \
                      AND status LIKE '{2}' \
                         ;".format(myGene, myTaxa, myCred)

        # execute query and collect results
        myQueryResults.extend(queryAndResults(args, myQuery))

    # print (write) output
    for fasta_header, aa_seq, gn, sp, uni, entry, st, ln in myQueryResults:
        print("{0}\n{1}".format(fasta_header, aa_seq))
        # additionally, write aa sequence, if write is True
        if args.write:
            myFile = "{0}/{1}mRNA".format(myDir, myFilePart)
            file = open(myFile, "a")
            file.write("{0}\n{1}\n".format(fasta_header, aa_seq))
            file.close()

    return myQueryResults

###############################################################################
# ENSEMBL
###############################################################################
    
def queryEnsembl(args):
    myGene = args.gene
    myTarget, myFilePart = createTarget(args)
    myQueryResults = []    # collect DB records of each query

    # prepare query for each element in myTarget
    for myTaxa in myTarget:
        myQuery = "SELECT canonical_gene_symbol, sp_name, ensembl.ensid, ens_trans_id \
                        , seqname, source, biotype \
                        , seq_region_start AS start, seq_region_end AS end, seq_region_strand as strand \
                     FROM ens_transcript \
                     JOIN ensembl USING (ensid) \
                     JOIN gene_symbols USING (uid) \
                     JOIN gene USING (gene_id) \
                     JOIN gene_symbols gs USING (gene_id) \
                     JOIN species USING (sp_name) \
                    WHERE gs.gene_symbol = '{0}' \
                      AND {1} \
                 ORDER BY sp_name, ensid, ens_trans_id;".format(myGene, myTaxa)

        # execute query and collect results
        myQueryResults.extend(queryAndResults(args, myQuery))

    # return results
    if myQueryResults:
        print("\ngene\tspecies\tENSEMBL_ID\ttranscript_ID\tseqname\tsource\tstart\tend\tstrand")
        for gs, sn, ei, ti, sqn, src, bt, start, end, strand in myQueryResults:
            print("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}".format(gs, sn, ei, ti, sqn, src, start, end, strand))
    else:
        print("\n\tNo transcripts found for {0} in selection.\n".format(myGene))

    return myQueryResults

###############################################################################
# Evidence
###############################################################################

def queryEvidence(args):
    myGene = args.gene
    myTarget, myFilePart = createTarget(args)
    myQueryResults = []    # collect DB records of each query

    print("myTarget: " + repr(myTarget))
    # prepare query for each element in myTarget
    for myTaxa in myTarget:
        if myGene == "ALL":
            myQuery = "SELECT DISTINCT gene_id, canonical_gene_symbol, gene_desc, ev_name, sp_order, \
                          sp_family, sp_name, sp_common, sp_short \
                     FROM evidence \
                     JOIN gene_symbols USING (uid) \
                     JOIN gene USING (gene_id) \
                     JOIN gene_symbols gs USING (gene_id) \
                     JOIN species USING (sp_name) \
                    WHERE 1 = 1 \
                      AND {1} \
                 ORDER BY sp_order, sp_family, sp_name;".format(None, myTaxa)
        else:
            myQuery = "SELECT DISTINCT gene_id, canonical_gene_symbol, gene_desc, ev_name, sp_order, \
                          sp_family, sp_name, sp_common, sp_short \
                     FROM evidence \
                     JOIN gene_symbols USING (uid) \
                     JOIN gene USING (gene_id) \
                     JOIN gene_symbols gs USING (gene_id) \
                     JOIN species USING (sp_name) \
                    WHERE gs.gene_symbol = '{0}' \
                      AND {1} \
                 ORDER BY sp_order, sp_family, sp_name;".format(myGene, myTaxa)

        # execute query and collect results
        myQueryResults.extend(queryAndResults(args, myQuery))

    # return results
    if myQueryResults:
        print("\ngene\tdescription\tevidence\torder\tfamily\tspecies\tcommon\tshort")
        for gene_id, gs, gd, en, so, sf, sn, sc, ss in myQueryResults:
            print("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}".format(gs, gd, en, so, sf, sn, sc, ss))
    else:
        print("\n\tNo evidence found for {0} in selection.\n".format(myGene))

    return myQueryResults


###############################################################################
# Species
###############################################################################

def querySpecies(args):
    myTarget, myFilePart = createTarget(args)
    myQueryResults = []    # collect DB records of each query

    # prepare ouput ordering
    if args.species or args.spcommon or args.genus or args.spclass:
        myOrder = "sp_name"
    elif args.family:
        myOrder = "sp_family, sp_name"
    elif args.order:
        myOrder = "sp_order, sp_family, sp_name"

    # prepare query for each element in myTarget
    for myTaxa in myTarget:
        myQuery = "SELECT sp_class, sp_order, sp_family, sp_genus, sp_name, sp_common, sp_short \
                     FROM species \
                    WHERE {0} \
                 ORDER BY {1};".format(myTaxa, myOrder)

        # execute query and collect results
        myQueryResults.extend(queryAndResults(args, myQuery))

    if myQueryResults:
        print("\norder\tfamily\tspecies\tcommon_name\tshort_name")
        for scl, so, sf, sg, sn, sco, ss in myQueryResults:
            print("{0}\t{1}\t{2}\t{3}\t{4}".format(so, sf, sn, sco, ss))
    else:
        print("\n\tSelection not found.\n")

    return myQueryResults


###############################################################################
# Gene
###############################################################################

def queryGene(args):
    myGene = args.gene
    # check for alternative gene_symbols (names)
    myQuery = "SELECT gene_symbol, " \
               "IF(gene_symbol=canonical_gene_symbol,gene_desc,CONCAT(canonical_gene_symbol,' synonym')) AS gene_desc, " \
               "uid, gene_id, gene_symbol<>canonical_gene_symbol AS is_synonym, '{0}' AS searched \
                 FROM gene_symbols gs JOIN gene USING(gene_id) \
                WHERE gene_id IN (SELECT gene_id \
                 FROM gene_symbols \
                WHERE gene_symbol = '{0}')".format(myGene)

    # execute query and collect results
    myQueryResults = queryAndResults(args, myQuery)

    # no alternative gene symbols found
    if len(myQueryResults)==1:
        myQueryResults=[]

    # return alternative gene symbols and descriptions (if present)
    if myQueryResults:
        print("\nAlternative gene symbols (names) found for {0}:\n\ngene symbol\tgene description:".format(myGene))
        for gene_symbol, gene_description, uid, gene_id, _, _ in myQueryResults:
            print("{0}\t\t{1} (uid {2})".format(gene_symbol, gene_description, uid))
        print("\n")

    return myQueryResults




#########################################
# execute from shell
#########################################
if __name__ == "__main__":
    args = parseArguments()
    
    if args.verbose:
        print("\nverbosity turned on")
        print("\nargparse:", args)
        print("\ndictionary:", vars(args))
        print("\nchosen sub-command:", args.sub_command)
    
    
    ###############################################################################
    # Assign variables
    ###############################################################################
    myTime = time.strftime('%Y%m%d_%H%M%S')
    myDir = "{0}_{1}".format(os.path.basename(sys.argv[0][:-3]), myTime)
    
  #  if args.verbose:
  #      print("\nDB connection:", myDB)
    
    
    ###############################################################################
    # If write is true, create directory for output files
    ###############################################################################
    if args.sub_command == 'b10k' or args.sub_command == 'uniprot':
        if args.write:
            try:
                os.mkdir(myDir)
            except FileExistsError:
                print("Directory ", myDir, " already exists.")
    
    
    
    ###############################################################################
    # Main
    ###############################################################################
    if args.sub_command == 'b10k':
        myQueryResults = queryB10k(args)
    elif args.sub_command == 'uniprot':
        myQueryResults = queryUniProt(args)
    elif args.sub_command == 'ensembl':
        myQueryResults = queryEnsembl(args)
    elif args.sub_command == 'evidence':
        myQueryResults = queryEvidence(args)
    elif args.sub_command == 'species':
        myQueryResults = querySpecies(args)
    
    if args.sub_command != 'species':
        myQueryResults = queryGene(args)
    
    ###############################################################################
    # User info and clean up
    ###############################################################################
    if args.sub_command == 'b10k' or args.sub_command == 'uniprot':
        if args.write:
            print("\n\tfiles written to directory", myDir, "\n")
    
    if args.verbose:
        print("\nquery results:", myQueryResults)
        print("\nnumber of records:", len(myQueryResults), "\n")
    
